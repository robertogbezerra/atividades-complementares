@extends('template')

@section('titulo','Adicionar Curso')

@section('page-header')
    <h1 class="h4 mb-0 text-gray-800">{{$curso->id == null ? 'Adicionar' : 'Editar'}} Curso</h1>
@endsection

@section('conteudo')

    <form method="POST" action="/adm/cursos/upsert">
        {{ csrf_field() }}
        <input type="hidden" value="{{ $curso->id }}" name="id">
        
        <div class="form-group">
        <label for="nomeCurso">Nome do Curso:</label>
        <input class="form-control" type="text" value="{{ $curso->nome }}" required name="nomeCurso">
        </div>

        <div class="form-group">
            <input type="submit" class="btn btn-primary" value="Salvar">
        </div>

    </form>

@endsection