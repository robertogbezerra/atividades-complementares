@extends('template')

@section('titulo','Configurações')

@section('page-header')
    <h1 class="h4 mb-0 text-gray-800">Configurações</h1>
@endsection

@section('conteudo')
<!-- a href="/adm/configuracoes/form/0" class="d-none d-inline-block btn btn-sm btn-primary shadow-sm mb-3"><i class="fas fa-user-plus fa-sm text-white-50"></i> Adicionar Configuração </a -->
    <!-- hr class="mt-0" -->
    @if(empty($configuracoes) or sizeof($configuracoes) == 0)
        Nenhuma configuração cadastrada.
    @else
<div class="table-responsive">
    <table class="table table-bordered" id="dataTable">
        <thead>
            <tr>
                <th class="col-2">Chave</th>
                <th class="col-3">Valor</th>
                <th class="col-5">Descrição</th>
                <th class="col-1">Editar</th>
            </tr>
        </thead>
        <tbody>
            @foreach($configuracoes as $configuracao)               
                <tr>
                    <td>{{ $configuracao->chave }} </td>
                    <td>{{ $configuracao->valor }} </td>
                    <td>{{ $configuracao->descricao }} </td>
                    <td >                        
                        <a href="/adm/configuracoes/form/{{ $configuracao->id }}" 
                        class="w-100 d-none d-sm-inline-block btn btn-sm btn-primary shadow-sm">
                            <i class="fas fa-edit fa-sm"></i> 
                        </a>
                    </td>
                </tr>

            @endforeach
        </tbody>
    </table>
</div>
@endif
@endsection