<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Login</title>

    <!-- Custom fonts for this template-->
    <link href="vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet">

    <!-- Custom styles for this template-->
    <link href="css/sb-admin-2.min.css" rel="stylesheet">

</head>

<body class="bg-gradient-primary">
<div class="container w-40 col-xl-8 col-lg-8 col-md-8 col-sm-8">
    <!-- Outer Row -->
    <div class="row justify-content-center">
        <div class="col-xl-8 col-lg-8 col-md-8">
            <div class="card o-hidden border-0 shadow-lg my-5">
                <div class="card-body p-0">
                    <!-- Nested Row within Card Body -->
                    <div class="row justify-content-center">
                        <div class=" w-100">
                            <div class="p-5">
                                <div class="text-center">
                                    <h1 class="h4 text-gray-900 mb-4">Bem Vindo!</h1>
                                </div>
                                <form class="user" action="/login" method="post">
                                    {{ csrf_field() }}
                                    <div class="form-group">
                                        <input type="text" name="login" class="form-control form-control-user" required="true" id="exampleInputEmail" aria-describedby="emailHelp" placeholder="Informe seu seu login...">
                                    </div>
                                    <div class="form-group">
                                        <input type="password" name="senha" class="form-control form-control-user" required="true" id="exampleInputPassword" placeholder="Informe sua senha...">
                                    </div>
                                    <div class="form-group">
                                        <div class="custom-control custom-checkbox small">
                                            <input type="checkbox" class="custom-control-input" id="customCheck">
                                            <label class="custom-control-label" for="customCheck">Remembrar Login</label>
                                        </div>
                                    </div>
                                    <input type="submit" value="Login" class="btn btn-primary btn-user btn-block"/>
                                    <hr>
                                    @if (session()->has('erro'))
                                        <div class="alert alert-danger mt-3 mb-0" role="alert">
                                            {{ session('erro') }}
                                        </div>
                                    @endif
                                </form>
                                <div class="text-center">
                                    <a class="small" href="forgot-password.html">Esqueceu a senha?</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="list-group w-100 px-1 text-center">
                <span class="list-unstyled py-2 text-center">
                    Desenvolvido pela turma da disciplina de Programação Web 
                </span>
            <span class="list-unstyled py-2">
                Curso: Análise e Desenvolvimento de Sistemas (Noite) 
            </span>
            <span class="list-unstyled py-2">
                Instituição: Faculdade CDL Fortaleza-CE
            </span>
        </div> 
    </div>
</div>
<!-- Bootstrap core JavaScript-->
<script src="vendor/jquery/jquery.min.js"></script>
<script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

<!-- Core plugin JavaScript-->
<script src="vendor/jquery-easing/jquery.easing.min.js"></script>

<!-- Custom scripts for all pages-->
<script src="js/sb-admin-2.min.js"></script>
</body>

</html>
