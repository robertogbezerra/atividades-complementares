@extends('template')

@section('titulo','Dashboard')

@section('page-header')
    <h1 class="h4 mb-0 text-gray-800">Dashboard</h1>
@endsection

@section('conteudo')

<div class="row justify-content-center">

        @component('components.smallCard')        
            @slot('iconHeader','graduation-cap')
            @slot('titulo','Certificados Pendentes')
            @slot('valor',"$certificadosPendentes")
            @slot('icon', 'fa fa-graduation-cap fa-5x')
            @slot('cor','warning')
            @slot('cardLink','/coordenador/certificado')
        @endcomponent

        @component('components.smallCard')
            @slot('iconHeader','file-alt')
            @slot('titulo','Relatórios')
            @slot('valor',"0")
            @slot('icon', 'fa fa-file-alt fa-5x')
            @slot('cor','info')
            @slot('cardLink','/coordenador/relatorio')
        @endcomponent

    </div>

@endsection

<script src="{{ asset('vendor/chart.js/Chart.min.js') }}"></script>
<script src="{{ asset('js/demo/chart-pie-demo.js') }}"></script>
