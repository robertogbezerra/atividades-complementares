@extends('template')

@section('titulo','Certificados Pendentes')

@section('page-header')
    <h1 class="h4 mb-0 text-gray-800">Certificados Pendentes</h1>
@endsection

@section('conteudo')
@if(empty($certificadosPendentes) or sizeof($certificadosPendentes) == 0)
        Nenhum cerficado pendente encontrado.
    @else
    <div class="table-responsive">
            <table class="table table-bordered" id="dataTable">
                <thead>
                    <tr>
                        <th>Curso</th>
                        <th>Instituição</th>
                        <th>Carga Horária</th>
                        <th>Aluno</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($certificadosPendentes as $certificado)
                       
                        <tr onclick="openCertificado({{ $certificado->id }})" style="cursor:hand">
                            <td>{{ $certificado->curso}} </td>
                            <td>{{ $certificado->instituicao}} </td>
                            <td>{{ $certificado->carga_horaria}} </td>
                            <td>{{ $certificado->aluno->nome}} </td>
                        </tr>

                    @endforeach
                </tbody>
            </table>
        </div>
@endif

@endsection

<script>
    function openCertificado(id){
        location.href = `certificado/form/${id}`;
    }
</script>
<script src="{{ asset('vendor/chart.js/Chart.min.js') }}"></script>
<script src="{{ asset('js/demo/chart-pie-demo.js') }}"></script>
