@extends('template')

@section('titulo','Sobre')

@section('page-header')
    <h1 class="h4 mb-0 text-gray-800">Sobre</h1>
@endsection

@section('conteudo')
    <div class="accordion" id="accordionExample">
        <div class="card">
            <div class="card-header bg-primary" id="headingOne">
                <h5 class="mb-0 ">
                <button class="btn text-white" type="button" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                    Sobre o Sistema
                </button>
                </h5>
            </div>
        
            <div id="collapseOne" class="collapse show" aria-labelledby="headingOne" data-parent="#accordionExample">
                <div class="card-body">
                    {{-- <div class="justify-content-around pb-3"> --}}
                        {{-- <span class="list-group-item list-group-item-action active">
                            Sistema de Atividades Complementares
                        </span> --}}
                        {{-- <div class="list-group-item"> --}}
                            <p class=" ml-2">
                                O presente trabalho tem por objetivo, fornecer a instituições de ensino, 
                                um controle das atividades complementares exigidas a seus alunos. <br> A ferramenta auxilia os coordenadores das instituições, nas tarefas diárias de controle dos certificados enviados pelos alunos,
                                agilizando as tarefas de inclusão, verificação e validação dos certificados enviados, e dando mais eficiência nesses processos.
                            </p>
                            <p class=" ml-2">
                               O Software dispõe de um sistema de mensagens intuitivo que facilita a comunicação entre coordenadores e alunos.
                               Com isso os usuários serão informados sobre qualquer evento, seja de envio, aprovação, reporvação ou atualização dos certificados.    
                            </p>
                            <p class=" ml-2">
                                 O sistema também permite aos coordenadores e alunos, terem um controle sobre todos os certificados enviados, fornecendo a estes uma Dashboard contendo relevantes a cada um como:  
                            </p>
                            <div class=" d-flex row justify-content-around ">
                                <input class="list-group-item" type="hidden" name="">
                                <span class="list-group-item list-group-item-action active">Dashboard para Alunos</span>
                                <ul class="w-100">
                                    <li class="py-2 text-left list-unstyled">
                                        <i class="fa fa-w-1 fa-arrow-alt-circle-right text-primary"></i> Quantidade de certificados aprovados.
                                    </li>
                                    <li class="py-2 text-left list-unstyled">
                                        <i class="fa fa-w-1 fa-arrow-alt-circle-right text-primary"></i> Quantidade de certificados reprovados.
                                    </li>
                                    <li class="py-2 text-left list-unstyled">
                                        <i class="fa fa-w-1 fa-arrow-alt-circle-right text-primary"></i> Quantidade horas já cursadas pelo aluno.
                                    </li>
                                    <li class="py-2 text-left list-unstyled">
                                        <i class="fa fa-w-1 fa-arrow-alt-circle-right text-primary"></i> Pontuação obtida até o presente momento
                                    </li>
                                    <li class="py-2 text-left list-unstyled">
                                        <i class="fa fa-w-1 fa-arrow-alt-circle-right text-primary"></i> E também poderá ver um gráfico com o percentual de pontos obtidos e exigidos.
                                    </li>
                                </ul>
                                <span class="list-group-item list-group-item-action active">Dashboard para Alunos</span>
                                <ul class="w-100">
                                    <li class="py-2 list-unstyled">
                                        <i class="fa fa-w-1 fa-arrow-alt-circle-right text-primary"></i> Quantidade de certificados aprovados.
                                    </li>
                                    <li class="py-2 list-unstyled">
                                        <i class="fa fa-w-1 fa-arrow-alt-circle-right text-primary"></i> Quantidade de certificados reprovados.
                                    </li>
                                    <li class="py-2 list-unstyled">
                                        <i class="fa fa-w-1 fa-arrow-alt-circle-right text-primary"></i> Quantidade horas já cursadas pelo aluno.
                                    </li>
                                    <li class="py-2 list-unstyled">
                                        <i class="fa fa-w-1 fa-arrow-alt-circle-right text-primary"></i> Pontuação obtida até o presente momento
                                    </li>
                                    <li class="py-2 list-unstyled">
                                        <i class="fa fa-w-1 fa-arrow-alt-circle-right text-primary"></i> E também poderá ver um gráfico com o percentual de pontos obtidos e exigidos.
                                    </li>
                                </ul>
                            </div>
                        {{-- </div> --}}
                    {{-- </div> --}}
                </div>
            </div>
        </div>
        <div class="card">
            <div class="card-header bg-primary" id="headingTwo">
                <h5 class="mb-0">
                <button class="btn collapsed text-white" type="button" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                    Equipe de Desenvolvimento
                </button>
                </h5>
            </div>
            <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordionExample">
                <div class="card-body">
                    <div class="pb-3 d-flex flex-xl-row flex-lg-row flex-md-row flex-sm-column">
                        <div class="mb-3 col-xl-6 col-md-6 col-sm-12 pr-lg-1 pr-md-1 pr-md-1 p-sm-0">
                            <span class="list-group-item list-group-item-action active">
                                Desenvolvimento e Design
                            </span>
                            <span  class="list-group-item">Amauri Matias 
                                <br> <span class="badge badge-primary badge-pill mb-1">Design</span>
                            </span>
                            <span  class="list-group-item">Dante Judson Gomes de Lima
                                <br> <span class="badge badge-primary badge-pill mb-1">Desenvolvimento</span>
                            </span>
                            <span  class="list-group-item">Paulo Allison Ferreira Xavier
                                <br> <span class="badge badge-primary badge-pill mb-1">Design</span>
                            </span>
                            <span  class="list-group-item">Roberto Gonçalves Bezerra
                                <div class="row ml-0 pt-1 mb-1">
                                    <br> <span class="badge badge-primary badge-pill mr-1 mb-1">Desenvolvimento</span>
                                    <br> <span class="badge badge-primary badge-pill mx-1 mb-1">Design</span>
                                </div>
                            </span>
                            <span  class="list-group-item">Sérgio Augusto Freitas Arciére
                                <br> <span class="badge badge-primary badge-pill mb-1">Desenvolvimento</span>
                            </span>
                        </div>
                        <div class="mb-3 col-xl-6 col-lg-6 col-md-6 col-sm-12 pl-lg-1 pl-md-1 pl-md-1 p-sm-0">
                                <span class="list-group-item list-group-item-action active">
                                    Documentação
                                </span>
                                <span  class="list-group-item ">Bruno Mesquita Monteiro
                                    <br> <span class="badge badge-primary badge-pill mb-1">Requisitos Funcionais</span>
                                </span>
                                <span  class="list-group-item">Davi da Silva Barbosa
                                    <br> <span class="badge badge-primary badge-pill mb-1">Revisão</span>
                                </span>
                                <span  class="list-group-item">Felipe Azin Moreira Lima
                                    <div class="row ml-0 pt-1 mb-1">
                                        <br> <span class="badge badge-primary badge-pill mr-1 mb-1">Requisitos Não Funcionais</span>
                                        <br> <span class="badge badge-primary badge-pill mr-1 mb-1">Revisão</span>
                                    </div>
                                </span>
                                <span  class="list-group-item ">Rodrigo da Costa Rodriques
                                    <div class="row ml-0 pt-1 mb-1">
                                        <br> <span class="badge badge-primary badge-pill mr-1 mb-1">Resivão Geral</span>
                                        <br> <span class="badge badge-primary badge-pill mx-1 mb-1">Requisitos não funcionais</span>
                                    </div>
                                </span>
                                <span  class="list-group-item">Samuel Carlos Ferreira Moura
                                    <br> <span class="badge badge-primary badge-pill mb-1">Especificação de Requisitos</span>
                                </span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
  </div>
@endsection