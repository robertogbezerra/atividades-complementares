<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use App\Rota;
use App\Perfil;

class RotasInsertPreviewPdf extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //coordenador/certificado/preview/pdf/*
        $coordenador = Perfil::where('valor','COORDENADOR')->first()->id;

        $rota = new Rota('coordenador/certificado/preview/pdf/*', $coordenador, null, null, false);
        $rota->save();
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
