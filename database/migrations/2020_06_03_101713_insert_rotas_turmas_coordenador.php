<?php

use App\Perfil;
use App\Rota;
use Illuminate\Database\Migrations\Migration;

class InsertRotasTurmasCoordenador extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $coordenador = Perfil::where('valor', 'COORDENADOR')->first()->id;

        $rota = new Rota('coordenador/cursos/meus-cursos', $coordenador, 'Meus Cursos', 'fa-book', true);
        $rota = new Rota('coordenador/cursos/*', $coordenador, null, null, false);
        
        $rota->save();
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        
    }
}
