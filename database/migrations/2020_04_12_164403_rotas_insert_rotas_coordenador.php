<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

use App\Perfil;
use App\Rota;

class RotasInsertRotasCoordenador extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $coordenador = Perfil::where('valor','COORDENADOR')->first()->id;
        
        $rota = new Rota('coordenador', $coordenador, 'Dashboard', 'fa-chart-pie', true);
        $rota->save();

        $rota = new Rota('coordenador/certificado', $coordenador, 'Certificados Pendentes', 'fa-graduation-cap', true);
        $rota->save();

        $rota = new Rota('coordenador/certificado/form/*', $coordenador, null, null, false);
        $rota->save();

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
