<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

use App\Rota;
use App\Perfil;

class RotasInsertDefaultValues extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $aluno = Perfil::where('valor','ALUNO')->first()->id;
        $coordenador = Perfil::where('valor','COORDENADOR')->first()->id;
        $administrador = Perfil::where('valor','ADMINISTRADOR')->first()->id;

        $rota = new Rota('aluno', $aluno, 'Dashboard', 'fa-chart-pie', true);
        $rota->save();

        $rota = new Rota('aluno/certificado', $aluno, 'Certificados', 'fa-graduation-cap', true);
        $rota->save();

        $rota = new Rota('adm/usuario', $administrador, 'Usuários', 'fa-user', true);
        $rota->save();

        $rota = new Rota('adm/usuario/upsert', $administrador, null, null, false);
        $rota->save();

        $rota = new Rota('adm/usuario/form/*', $administrador, null, null, false);
        $rota->save();
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
