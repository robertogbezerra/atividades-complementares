<?php

use App\Perfil;
use App\Rota;
use Illuminate\Database\Migrations\Migration;

class InsertRotasTurmasAdmin extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $admin = Perfil::where('valor', 'ADMINISTRADOR')->first()->id;

        $rota = new Rota('adm/turmas', $admin, 'Turmas', 'fa-users', true);
        $rota = new Rota('adm/turmas/adicionar', $admin, null, null, false);
        $rota = new Rota('adm/turmas/editar/*', $admin, null, null, false);
        $rota = new Rota('adm/turmas/upsert', $admin, null, null, false);
        
        $rota->save();
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
