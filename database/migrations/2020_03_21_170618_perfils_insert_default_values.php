<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use App\Perfil;

class PerfilsInsertDefaultValues extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $perfil = new Perfil();
        $perfil->valor = 'ALUNO';
        $perfil->descricao = 'Perfil que será cedido aos alunos: acesso ao painel de alunos';
        $perfil->save();
        
        $perfil = new Perfil();
        $perfil->valor = 'COORDENADOR';
        $perfil->descricao = 'Perfil que será cedido aos funcinarios da cdl com permissão para aprovar ou rejeitar certificados.';
        $perfil->save();

        $perfil = new Perfil();
        $perfil->valor = 'ADMINISTRADOR';
        $perfil->descricao = 'Perfil que será cedido ao funcinario da cdl com permissão para aprovar ou rejeitar certificados e/ou alteração .';
        $perfil->save();

        
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        
        
    }
}
