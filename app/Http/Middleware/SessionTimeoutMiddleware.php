<?php

namespace App\Http\Middleware;

use App\Configuracao;
use Closure;
use Illuminate\Support\Facades\Config;

class SessionTimeoutMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
            //pega a configuração de timeout do bd
            $config = Configuracao::where('chave', 'session_timeout')->firstOrFail();
            //pega o tempo que está na configuração do bd e coloca no timeout da sessão
            Config::set('session.lifetime', $config->valor); 
            $timeout = $config->valor;
            session()->put('timeout', $timeout);  
            return $next($request);
        
    }
}
