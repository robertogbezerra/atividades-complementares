<?php

namespace App\Http\Middleware;

use Closure;

use App\Mensagem;

class MessageMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {

        if (session()->has('usuarioLogado')) {
            $usuarioId = session('usuarioLogado')->id;
            $mensagens = Mensagem::getMensagensAgrupadasPorRemetente($usuarioId);
            $mensagensNaoLidas = Mensagem::getMensagensNaoLidasCount($usuarioId);

            session(['mensagens' => $mensagens]);
            session(['mensagensNaoLidas' => $mensagensNaoLidas]);
        }

        return $next($request);
    }
}
