<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Configuracao;                            
use App\Usuario;
use Exception;

class LoginController extends Controller
{
    public function login(Request $request) {
        session()->flush();

        $login = $request->input('login');
        $senha = $request->input('senha');

        try {

            $usuario = Usuario::login($login, $senha);
            unset($usuario->senha);
            session(['usuarioLogado'=>$usuario]);
            return redirect($usuario->getDefaultRoute());     

        } catch (Exception $e) {
            return back()->with('erro',$e->getMessage());
        }

    }

    public function logoff() {
        session()->flush();
        return redirect('/');
    }

    public function index() {
        if(session()->has('usuarioLogado')) {
            $usuario = session('usuarioLogado');
            return redirect($usuario->getDefaultRoute());
        }
        return view('login');
    }
}
