<?php

namespace App\Http\Controllers;

use App\Curso;
use App\Enums\TurnoCursoEnum;
use App\Turma;
use App\Usuario;
use Illuminate\Database\QueryException;
use Illuminate\Http\Request;

class TurmaController extends Controller
{
    public function listarTurmas(Request $request) {
        $coordenadores = Usuario::where('perfil_id', 2)->get()->all();
        $listTurmas = array();
        foreach ($coordenadores as $c => $coordenador) {
            $turmas = $coordenador->cursosWithPivot($coordenador->id);
            foreach ($turmas as $t => $turma) {
                array_push(
                    $listTurmas, [$turma, $coordenador->nome]
                );
            } 
        }

        return view('adm.turmas.list', compact('listTurmas'));
    }
   
    public function formTurma(Request $request) {
        $path = $request->path();
        $content = 'adicionar';
        $turma = Turma::find($request->id);
        $cursos = Curso::all();
        $usuarios = Usuario::where(['perfil_id' => 2])->get()->all();
        $turnos = TurnoCursoEnum::$turnos;
        
        if (strpos($path, $content)) {
            $turma = new Turma(); 
        }
        else if ($turma == null) {
            $turma = Turma::find($request->id);
            return redirect('adm/turmas')->with('erro', 'Turma não encontrada!');
        }
        return view('adm.turmas.form', compact('turma', 'turnos', 'cursos', 'usuarios'));
        
    }
    
    public function upsertTurma(Request $request) {
        $turmaBD = $this->cursoExists($request);
        $turma = new Turma();
        if ($turmaBD == null) {
            try {
                $turma = $this->preencheTurma($request, $turma);                
                $turma->save();
                
                $mensagem = 'Turma ' . ($request->input('id') != null ? 'alterada' : 'inserida') . ' com sucesso';
                return redirect('adm/turmas')->with('mensagem', $mensagem);
    
            } catch (QueryException $e) {
                error_log($e->getMessage());
                return redirect()->back()->with('erro', 'Houve um errro ao inserir o registro!');
            }
        } else {
            return redirect()->back()->with('erro', 'Coordenador já cadastrado para essa turma!');
        }
        
    }

    public function preencheTurma($request, $turma)
    {      
        if ($request->input('id') != null) {
            $turma = Turma::find($request->input('id'));
        }
        $turma->usuario_id = $request->input('usuario_id');
        $turma->curso_id = $request->input('curso_id');
        $turma->turno = $request->input('turno'); 
        
        return $turma;
    }

    public function cursoExists($request) {

        return $turmaBD = Turma::where(
            [
                'curso_id' => $request->input('curso_id'),
                'usuario_id' => $request->input('usuario_id'), 
                'turno' => $request->input('turno')
            ]
        )->first();

    }
}
