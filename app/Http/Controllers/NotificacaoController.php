<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Database\QueryException;

use App\Notificacao;

class NotificacaoController extends Controller
{
    public function read($id, Request $request) {
        try {
            $notificacao = Notificacao::find($id);
            $notificacao->visto = true;
            $notificacao->save();
            return response(Notificacao::where('visto',false)->count(),200); 
        } catch (QueryException $e) {
            error_log($e->getMessage());
        }
    }
}
