<?php

namespace App\Http\Controllers;

use App\Certificado;
use App\Configuracao;
use App\Curso;
use Illuminate\Http\Request;
use Illuminate\Database\QueryException;
use App\Enums\CertificadoStatusEnum;

class AlunoController extends Controller
{
    public function dashboard() {
        $alunoId = session('usuarioLogado')->id;
        $certificadosPendentes = Certificado::countCertificadosPendentesPorAlunoId($alunoId);
        $totalHoras = Certificado::getTotalDeHorasPorAlunoId($alunoId);
        $pontuacao = Certificado::getPontuacaoPorAlunoId($alunoId);      
        session(['pontos-ganhos' => $pontuacao]); 
        return view('aluno.dashboard', compact('certificadosPendentes', 'totalHoras', 'pontuacao'));
    }

    public function certificadoForm() {
        return view('aluno.form_certificado');
    }

    public function listarCertificados() {
        $alunoId = session('usuarioLogado')->id;
        $certificados = Certificado::getCertificadosPorAluno($alunoId);
        return view('aluno.certificado.list', compact('certificados'));
    }

    public function enviarCertificado(Request $request) {

        try {
            $certificado = new Certificado();
            $usuarioId = session('usuarioLogado')->id;
            $certificado->status = CertificadoStatusEnum::PENDENTE;
            $certificado->curso = $request->input('curso');
            $certificado->instituicao = $request->input('instituicao');
            $certificado->carga_horaria = $request->input('carga_horaria');
            $certificado->caminho_arquivo = $request->file('caminho_arquivo')->storeAs('pdf', Certificado::generateFileName(20));
            $certificado->data_conclusao = $request->input('data_conclusao');
            $certificado->aluno_id = $usuarioId;
            $certificado->data_envio = date('Y-m-d');
            $certificado->cod_verificacao = $request->input('cod_verificacao');
            $certificado->validaDataConclusao();
            $certificado->save();
            return redirect('/aluno')->with('mensagem','Certificado enviado com sucesso');
            

        } catch (QueryException $e) {
            error_log($e->getMessage());
            return redirect()->back()->with('erro', 'Houve um erro ao inserir o registro!');
        } catch (Exception $e) {
            error_log($e->getMessage());
            return redirect()->back()->with('erro', $e->getMessage());
        }
    }

}
