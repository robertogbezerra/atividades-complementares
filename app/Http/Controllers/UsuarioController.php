<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Usuario;
use App\Perfil;
use Illuminate\Database\QueryException;

class UsuarioController extends Controller
{
    public function listaUsuarios() {
        $usuarios = Usuario::all();
        return view('adm.usuario.list',compact('usuarios'));
    }


    public function formUsuario($id = null) {
        $usuario = new Usuario();
        $perfils = Perfil::all();
        if ($id != null) {
            $usuario = Usuario::find($id);

            if ($usuario == null) {
                //TODO redirect to 404 NOT FOUND
            }

        }

        return view('adm.usuario.form', compact('usuario','perfils'));
    }

    public function upsert(Request $request) {

        try {
            $usuario = new Usuario();
    
            if ($request->input('id') != null) {
                $usuario = Usuario::find($request->input('id'));
            }
            $usuario->login = $request->input('login');
            $usuario->cpf = $request->input('cpf');
            $usuario->nome = $request->input('nome');
            $usuario->email = $request->input('email');
            $usuario->perfil_id = $request->input('perfil_id');
            $usuario->save();
    
            $mensagem = 'Usuário ' . ($request->input('id') != null ? 'alterado' : 'inserido') . ' com sucesso';
            
            $usuarios = Usuario::all();
    
            return redirect('/adm/usuario')->with('mensagem',$mensagem);
        } catch (QueryException $e) {
            error_log($e->getMessage());
            return redirect()->back()->with('erro', 'Houve um errro ao inserir o registro!');
        }
    }
}
