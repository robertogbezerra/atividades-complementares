<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;


use Storage;
use Response;
use App\Certificado;
use App\Enums\CertificadoStatusEnum;
use App\Enums\CertificadoMotivoRecusaEnum;
use PDF;
use App;

class CoordenadorController extends Controller
{
    public function dashboard() {
        $certificadosPendentes = Certificado::where('status',CertificadoStatusEnum::PENDENTE )->count();
        return view('coordenador.dashboard',compact('certificadosPendentes'));
    }

    public function listaCertificadosPendentes() {
        $certificadosPendentes = Certificado::where('status', CertificadoStatusEnum::PENDENTE)->get();
        return view('coordenador.certificados.pendentes', compact('certificadosPendentes'));
    }

    public function formCertificados($certificadoId) {
        $certificado = Certificado::find($certificadoId);

        return view('coordenador.certificados.form', compact('certificado'));
    }

    public function aprova($certificadoId) {
        $certificado = Certificado::find($certificadoId);
        $coordenadorId = session('usuarioLogado')->id;
        $certificado->status = CertificadoStatusEnum::APROVADO;
        $certificado->data_avaliado = date('Y-m-d');
        $certificado->coordenador_id = $coordenadorId;

        $certificado->save();

        return redirect('/coordenador/certificado')->with('mensagem','Certificado aprovado com sucesso!');
    }

    public function reprova($certificadoId, Request $request) {
        try {
            $certificado = Certificado::find($certificadoId);
            $coordenadorId = session('usuarioLogado')->id;
            $certificado->status = CertificadoStatusEnum::REPROVADO;
            $certificado->data_avaliado = date('Y-m-d');
            $certificado->motivo_recusa = CertificadoMotivoRecusaEnum::getEnumByKey($request->input('motivo_recusa'));
            $certificado->observacao = $request->input('observacao');
            $certificado->coordenador_id = $coordenadorId;
    
            $certificado->save();
    
            return redirect('/coordenador/certificado')->with('mensagem','Certificado REPROVADO com sucesso!');
        } catch (Exception $e) {
            error_log($e->getMessage());
        }
    }

    public function pdfPreview($id) {
        $filePath = Certificado::findOrFail($id)->caminho_arquivo;
        if( ! Storage::exists($filePath) ) {
            abort(404);
          }
      
          $pdfContent = Storage::get($filePath);
      
          return Response::make($pdfContent, 200, [
            'Content-Type'        => 'application/pdf',
            'Content-Disposition' => 'filename="preview.pdf"'
          ],'inline');
    }

    public function relatorio(Request $request) {
        $whereArray = [];
        $certificados = [];
        $filter = [];
        
        if (sizeof($request->all()) > 0) {
            $whereArray = $this->getWhereFromRequest($request);
            $certificados = Certificado::where($whereArray)->get();    
        }

        $filter = $this->getFieldsFromRequest($request);

        return view('coordenador.relatorio.relatorio',compact('filter','certificados'));
    }

    private function getFieldsFromRequest($request) {
        $filter = [];

        $filter['status'] = $request->input('status');
        $filter['motivo_recusa'] = $request->input('motivo_recusa');
        $filter['curso'] = $request->input('curso');
        $filter['instituicao'] = $request->input('instituicao');
        $filter['carga_horaria'] = $request->input('carga_horaria');

        return $filter;
    }

    private function getWhereFromRequest($request) {
        $whereArray = [];

            if ($request->input('curso') != null){
                array_push($whereArray, ['curso','like', '%'.$request->input('curso').'%']);
            }
            if ($request->input('instituicao') != null){
                array_push($whereArray, ['instituicao','like', '%'.$request->input('instituicao').'%']);
            }
            if ($request->input('carga_horaria') != null){
                array_push($whereArray, ['carga_horaria', $request->input('carga_horaria')]);
            }
            if ($request->input('motivo_recusa') != null){
                array_push($whereArray, ['motivo_recusa', $request->input('motivo_recusa')]);
            }
            if ($request->input('status') != null){
                array_push($whereArray, ['status', $request->input('status')]);
            }
        
        return $whereArray;    

    }

    public function exportRelatorio(Request $request) {

        $dataGerado = date('d/m/Y H:i:s');
        $geradoPor = session('usuarioLogado')->nome;
        $whereArray = $this->getWhereFromRequest($request);
        $filter = $this->getFieldsFromRequest($request);
        $certificados = Certificado::where($whereArray)->get();    
        $pdf = App::make('dompdf.wrapper');

        $pdf->getDomPDF()->set_option("enable_php", true);
        $pdf->loadView('coordenador.relatorio.export',compact('certificados','dataGerado','geradoPor','filter'));
        $pdf->setPaper('A4', 'landscape');
        return $pdf->download('relatorio'.$dataGerado.'.pdf');
        // return view('coordenador.relatorio.export',compact('certificados','dataGerado','geradoPor','filter'));
    }
}
