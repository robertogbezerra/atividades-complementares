<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Curso;
use App\Turma;
use App\Usuario;
use Illuminate\Database\QueryException;

class CursoController extends Controller
{
    public function listarCursosAdmin(Request $request) {
        $cursos = Curso::all();
        return view('adm.cursos.list', compact('cursos'));
    }
   
    public function formCursosAdmin(Request $request) {
        $path = $request->path();
        $content = 'adicionar';
        $curso = Curso::find($request->id);
        if (strpos($path, $content)) {
            $curso = new Curso();
        }
        else if ($curso == null) {
            return redirect('adm/cursos')->with('erro', 'Curso não encontrado!');
        }
        return view('adm.cursos.form', compact('curso'));  
    }
    
    public function upsertCurso(Request $request) {
        $curso = new Curso();
        try {
            $curso = $this->preencheCurso($request, $curso);
            $curso->save();
            $mensagem = 'Curso ' . ($request->input('id') != null ? 'alterado' : 'inserido') . ' com sucesso';
            return redirect('adm/cursos')->with('mensagem', $mensagem);

        } catch (QueryException $e) {
            error_log($e->getMessage());
            return redirect()->back()->with('erro', 'Houve um errro ao inserir o registro!');
        }
    }
    
    public function listarAlunosAdmin(Request $request) {
        $curso = Curso::where('nome', $request->nomeCurso)->get()->first();
        $mensagem = 'Desculpe, O curso solicitado não existe na sua grade de cursos. Veja seus cursos abaixo ';
        
        if ($this->validaCursoRequest($request, $curso, $this->getCoordenador()->id)) {
            $alunos = $curso->getAlunosByCurso(
                $this->getCoordenador()->id, $curso->id, $request->turno
            ); 
            return view('adm.cursos.list', compact('alunos', 'curso'));
        }
        else return redirect('adm/cursos/alunos')->with('erro', $mensagem);
    }

    public function iserirCurso(Request $request) {
      return 0;
    }

    public function listarTurmasCoordenador(Request $request) {
        $coordenador = Usuario::find($this->getCoordenador()->id);
        $cursos = $coordenador->cursosWithPivot($coordenador->id);
        $cores = $this->getColors(count($cursos));
        $qtdeAlunos = [];
        foreach ($cursos as $c => $cursoAtual) {
            array_push(
                $qtdeAlunos, count(
                    $cursoAtual->getAlunosByCurso(
                        $this->getCoordenador()->id, $cursoAtual->id, $cursoAtual->pivot->turno
                    )
                )
            );     
        }
        return view('coordenador.cursos.meus-cursos', compact('cursos', 'qtdeAlunos', 'cores'));
    }

    public function listarAlunos(Request $request) {
        $curso = Curso::where('nome', $request->nomeCurso)->get()->first();
        $turma = 
        $mensagem = 'Desculpe, O curso solicitado não existe na sua grade de cursos. Veja seus cursos abaixo ';
        
        if ($this->validaCursoRequest($request, $curso, $this->getCoordenador()->id)) {
            $alunos = $curso->getAlunosByCurso(
                $this->getCoordenador()->id, $curso->id, $request->turno
            ); 
            return view('coordenador.cursos.list', compact('alunos', 'curso'));
        }
        else return redirect('coordenador/cursos/meus-cursos')->with('erro', $mensagem);
    }

    public function preencheCurso($request, $curso) {

        if ($request->input('id') != null) {
            $curso = Curso::find($request->input('id'));
        }
        $curso->nome = $request->input('nomeCurso');

        return $curso;
    }

    /**
     * Valida os dados da requisição do curso
     * 
     * Verifica se o curso requisitado pelo coordenador existe
     * Se o curso existir na sua grade de cursos retorna true
     * se não retorna falso.
     * 
     * @param  Request $request requisição passada pelo usuário
     * @param  Curso  $curso instância da Classe Curso, o curso requisitado pelo usuário.
     * @param  int $coordenadorId ID do Coordenador
     * @return bool  resulta 'true' se o curso é válido, do contrário resulta ems 'false'.
     **/
    public function validaCursoRequest($request, $curso, $coordenadorId) {

        if ($curso != null) {
            $cursoRequest = Turma::where(
                [
                    'usuario_id' => $coordenadorId,
                    'curso_id' => $curso->id,
                    'turno' => $request->turno
                ] 
            )->get()->all();
            if ($cursoRequest != null) 
                return true;
            else 
                return false;
        }
    }

    /**
     * Retorna um objeto com os dados do coordenador logado no sistema
     *
     * @return Usuario
     **/
    public function getCoordenador() {
        return session('usuarioLogado');
    }

     /**
     * Define as cores que serão usadas nos cards
     *
     *
     * @param int $tam tamanho do vet (quantidade de cards)
     * @return array strings com nomes das cores
     **/
    public static function getColors($tam) {
        $opcoes = array( 'success', 'primary', 'info',
        'danger', 'warning', 'dark', 'secondary' );

        $cor = random_int(0, (count($opcoes) - 1));

        for ($i=0; $i < $tam; $i++) { 
            $cores[$i] = $opcoes[$cor];
            if ($i > 0) {
                while ($cores[$i - 1] == $cores[$i]) {
                    $cor = random_int(0, (count($opcoes) - 1));
                    $cores[$i] = $opcoes[$cor];
                }
            }
        }
        
        return $cores;
    }
}
