<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Rota extends AuditableModel
{
 
    function __construct() {

        if (func_num_args() == 5) {
            $this->path = func_get_arg(0);
            $this->perfil_id = func_get_arg(1);
            $this->label = func_get_arg(2);
            $this->icone = func_get_arg(3);
            $this->is_menu = func_get_arg(4);
        }
    }
}
