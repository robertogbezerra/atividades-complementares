<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;


class Mensagem extends AuditableModel
{
    public function remetente() {
        return $this->belongsTo('App\Usuario','from');
    }

    public static function getMensagensAgrupadasPorRemetente($usuarioId) {
        return DB::select('select nome, mensagem 
                            from mensagems m1, usuarios
                            where `to` = ? 
                            and usuarios.id = `from`
                            and m1.created_at = (select max(created_at) 
                                                from mensagems m2 
                                                where m1.`to` = m2.`to` 
                                                and m1.`from` = m2.`from`)', array($usuarioId));
    }

    public static function getMensagensNaoLidasCount($usuarioId) {
        return Mensagem::where('to',$usuarioId)
        ->where('visto', false)
        ->count();
    }
}
