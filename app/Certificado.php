<?php

namespace App;

use Illuminate\Http\Request;
use App\Notificacao;
use App\Enums\CertificadoStatusEnum;

class Certificado extends AuditableModel 
{

    public static function boot() {
        
        parent::boot();
        
        self::updated(function ($model) {
            if ($model->status == CertificadoStatusEnum::APROVADO ) {
                $notificacao = new Notificacao('Seu certificado enviado em '.$model->data_envio.' acaba de ser aprovado.',$model->aluno_id);
                $notificacao->save();
            }

            if ($model->status == CertificadoStatusEnum::REPROVADO ) {
                $notificacao = new Notificacao(
                    'Seu certificado enviado em '.$model->data_envio." acaba de ser reprovado.\nMotivo: " . $model->motivo_recusa . "\nObservação: ".$model->observacao
                    ,$model->aluno_id);
                $notificacao->save();
            }
        });

    }

    public static function verificaExtensao(Request $req){
        $path = $req->input('arquivo');
        $ext = pathinfo($req->input('arquivo'), PATHINFO_EXTENSION);
        return $ext;
    }

    public function aluno() {
        return $this->belongsTo('App\Usuario','aluno_id');
    }

    public function coordenador() {
        return $this->belongsTo('App\Usuario','coordenador_id');
    }

    public function validaDataConclusao() {
        $today = date("Y-m-d"); 
        if($today < $this->data_conclusao) {
            throw new Exception('Data de conclusão não pode ser futura.');
        }
    }

    public static function generateFileName($length = 10) {
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }
        return $randomString;
    }


    public static function getCertificadosPorAluno($alunoId) {
        return Certificado::where('aluno_id', $alunoId)->get();
    }

    public static function getCertificadosAprovadosPorAlunoId($alunoId) {
        return Certificado::where('aluno_id', $alunoId)
        ->where('status', CertificadoStatusEnum::APROVADO)
        ->get();
    }

    public static function countCertificadosPendentesPorAlunoId($alunoId) {
        return Certificado::where('aluno_id', $alunoId)
        ->where('status', CertificadoStatusEnum::PENDENTE)
        ->count();
    }

    public static function getTotalDeHorasPorAlunoId($alunoId) {
        return Certificado::where('aluno_id', $alunoId)
        ->where('status', CertificadoStatusEnum::APROVADO)
        ->sum('carga_horaria');
    }

    public static function getPontuacaoPorAlunoId($alunoId) {
        $certificados = Certificado::getCertificadosAprovadosPorAlunoId($alunoId);
        
        $pontuacao = 0;

        foreach ($certificados as $certificado) {
            if ($certificado->carga_horaria < 5) {
                $pontuacao += 5;
            } else if ($certificado->carga_horaria < 10) {
                $pontuacao += 10;
            } else if ($certificado->carga_horaria < 16) {
                $pontuacao += 15;
            } else {
                $pontuacao += 20;
            }
        }

        return $pontuacao;
    }

}
