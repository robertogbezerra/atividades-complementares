<?php

namespace App;

class Notificacao extends AuditableModel
{
    function __construct() {

        if (func_num_args() == 2) {
            $this->mensagem = func_get_arg(0);
            $this->to = func_get_arg(1);
        }

    }
 
    public static function getNotificacoesNaoLidas($usuarioId) {
        return Notificacao::where('to', $usuarioId)
            ->where('visto',false)
            ->get();   
    }
}