<?php

namespace App\Enums;

class CertificadoStatusEnum {
    public const PENDENTE = 'PENDENTE';
    public const APROVADO = 'APROVADO';
    public const REPROVADO = 'REPROVADO';
}