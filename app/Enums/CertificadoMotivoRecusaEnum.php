<?php

namespace App\Enums;

class CertificadoMotivoRecusaEnum {
    public const DADOS_ERRADOS = 'DADOS ERRADOS';
    public const FORA_DO_PRAZO = 'FORA DO PRAZO';
    public const INVALIDO_PARA_CURSO = 'INVÁLIDO PARA CURSO';

    public static function getEnumByKey($value) {
        if ($value == 'DADOS_ERRADOS') {
            return CertificadoMotivoRecusaEnum::DADOS_ERRADOS;
        }

        if ($value == 'FORA_DO_PRAZO') {
            return CertificadoMotivoRecusaEnum::FORA_DO_PRAZO;
        }

        if ($value == 'INVALIDO_PARA_CURSO') {
            return CertificadoMotivoRecusaEnum::INVALIDO_PARA_CURSO;
        }
        
        throw new Exception('Valor inválido para classe CertificadoMotivoRecusaEnum');
    }
}