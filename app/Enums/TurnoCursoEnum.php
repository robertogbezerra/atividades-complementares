<?php

namespace App\Enums;

class TurnoCursoEnum {
    public static $turnos = array(
        'MANHÃ', 'TARDE', 'NOITE'
    );
}