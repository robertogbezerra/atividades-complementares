<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Curso extends AuditableModel
{
    function __construct() {

        if (func_num_args() == 1) {
            $this->nome = func_get_arg(0);
        }
    }

    public function getAlunosByCurso($coordenador_id, $cursoId, $turno) {
        return $this->belongsToMany(Usuario::class, 'turmas')
            ->where(
                [   
                    ['turmas.usuario_id', '<>', $coordenador_id],
                    'turmas.curso_id' => $cursoId, 
                    'turmas.turno' => $turno
                ]
            )
            ->get(
                [
                    '*', 'turno'
                ]
            );
    }

    public function alunos() {
        return $this->belongsToMany(Usuario::class, 'turmas');
    }
}