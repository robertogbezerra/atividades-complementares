// Set new default font family and font color to mimic Bootstrap's default styling
// Chart.defaults.global.defaultFontFamily = 'Nunito', '-apple-system,system-ui,BlinkMacSystemFont,"Segoe UI",Roboto,"Helvetica Neue",Arial,sans-serif';
// Chart.defaults.global.defaultFontColor = '#858796';

// Pie Chart Example
document.addEventListener("DOMContentLoaded", function(event) { 
  var ctx = document.getElementById("grafico");
  var pontosGanhos = parseInt(document.querySelector('#pontos-ganhos').value);
  var pontosExigida = parseInt(document.querySelector('#pontuacao-exigida').value);
  var grafico = new Chart(ctx, {
    type: 'pie',
    data: {
      labels: ["Concluído", "Exigido"],
      datasets: [{
        data: [calculaPercent(pontosExigida, pontosGanhos), calculaPercent(pontosExigida, (pontosExigida - pontosGanhos))],
        backgroundColor: ['#1870f9c9', '#bdc8c4'],
        hoverBackgroundColor: ['#0a59bf', '#a7acaa'],
        hoverBorderColor: "rgba(234, 236, 244, 1)",
      }],
    },
    options: {
      maintainAspectRatio: false,
      tooltips: {
        backgroundColor: "rgb(255,255,255)",
        bodyFontColor: "#858796",
        borderColor: '#dddfeb',
        borderWidth: 1,
        xPadding: 15,
        yPadding: 15,
        displayColors: false,
        caretPadding: 10,
      },
      legend: {
        display: true
      },
      cutoutPercentage:70,
    }
  });
});

function calculaPercent(fator, valor) {
  return  valor = ((valor / fator) * 100).toPrecision(3);
}
